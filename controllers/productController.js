const Product = require("../models/Products.js");
// const Order = require("../models/Order")



// creating product
module.exports.addProduct = (data) => {
	console.log(data.isAdmin);
	if(data.isAdmin){
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price,
			stocks: data.product.stocks

		})
		return newProduct.save().then((data, error) => {
			if(error){
				return error;
			}
			return data;
		})
	}
	let message = Promise.resolve("You don't have an access to create a product.");

	return message.then((value) => {
		return value
	})
};


// get all product
module.exports.allProduct = () => {

	return Product.find({}).then(products => {
		return products;
	})	
};

// module.exports.allProduct = (reqBody, isAdmin) => {
// 	if(isAdmin){
// 		return Product.find({}).then(products => {
// 		return products;
// 	})	
// 	}
// 	let message = Promise.resolve("You don't have an access to view all product");

// 	return message.then((value) => {
// 		return value
// 	})
// };


// get all active product /mod

module.exports.allProductActive = () => {
	return Product.find({isActive: true}).then(active => {
		return active;
	})
};

module.exports.allActive = (isAdmin) => {
	if(isAdmin){
		return Product.find({isActive: true}).then(active => {
		return active;
	})
	}
	let message = Promise.resolve("You don't have an access to view all active product");

	return message.then((value) => {
		return value
	})
};






// get single product
module.exports.getSingle = (reqParams) => {
	return Product.findById(reqParams.productId).then(get => {
		return get;
	})
};


// Update Product
module.exports.updateProduct = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);
	if(isAdmin){
		let updatedProduct = {
			name : reqBody.name,
		description: reqBody.description,
		price : reqBody.price,
		stocks : reqBody.stocks,

	
		}
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((update, error) => {
			if(error){
				return false;
			}else{
				return "Product Updated!"
			}
		})
	}

	let message = Promise.resolve("No access to update the product.");
	return message.then((value) => {
		return value;
	})
	
};

// delete or archiving product

module.exports.deleteProduct = (reqParams, reqBody, isAdmin) =>{
	console.log(isAdmin);
	if(isAdmin){
		let deletedProduct = {
			isActive: reqBody.isActive
		}
		return Product.findByIdAndUpdate(reqParams.productId, deletedProduct).then((remove, error) =>{
			if(error){
				return false;
			}else{
				return "Product has been deleted."
			}
		})
	}
	let message = Promise.resolve("You don't have the access to delete the products");

	return message.then((value) => {
		return value
	})
};

// remove product
module.exports.archiveProduct = (reqParams, reqBody, isAdmin) =>{
	console.log(isAdmin);
	if(isAdmin){
		return Product.findByIdAndRemove(reqParams.productId).then(del => {
			return del;
		})
	}else{
		return false;
	}
}

// activate product only
module.exports.allActiveAdmin = (isAdmin) => {
	if(isAdmin){
		return Product.find({isActive: true}).then(active => {
		return active;
	})
	}
	let message = Promise.resolve("You don't have an access to view all active product");

	return message.then((value) => {
		return value
	})
};


